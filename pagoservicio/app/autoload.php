<?php

	#esta funcion elimina el hecho de estar agregando los modelos manualmente
	function abisoft_autoload($modelname){
		if(Model::exists($modelname)){
			include Model::getFullPath($modelname);
		} 
	}

	spl_autoload_register("abisoft_autoload");

?>