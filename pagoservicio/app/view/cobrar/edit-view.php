<?php 
    if(!isset($_SESSION["user_id"])){ Core::redir("./");}
    $user_session= UserData::getById($_SESSION["user_id"]);
    // si el id  del usuario no existe.
    if($user_session==null){ Core::redir("./");}
?>
<?php 
    $cliente = ClienteData::getById(intval($_GET["id"]));
    if($cliente==null){ Core::redir("cliente/index");}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content"><!-- Main content -->
		<div class="row">
            <div class="col-md-12">
                <div class="box">
                <!-- <form> -->
                <form role="form" action="./?action=cobrar&type=store&id=<?php echo $cliente->cliente_id;?>" method="POST">
                    <div class="box-header with-border">
                        <h1 class="box-title">Cobrar</h1>
                        <div class="box-tools pull-right"></div>
                    </div>
                    <div class="panel-body">
                    	<?php if(isset($_SESSION['data'])){ ?>
                            <div class="alert alert-<?php echo $_SESSION['data']['alert']; ?> fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong><?php echo $_SESSION['data']['notice']; ?></strong> <?php echo $_SESSION['data']['msg']; ?>
                            </div>
                        <?php 
                                unset($_SESSION['data']);//elimino la variable de sesion
                            } 
                        ?>
						<div class="row">
				            <div class="col-md-6">
				              	<div class="well">
				                	<p>
				                  		<strong>CLIENTE:</strong> <?php echo $cliente->nombre; ?><br>
				                  		<strong>DIRECCIÓN:</strong> <?php echo $cliente->direccion; ?><br>
				                  		<strong>TELEFONO:</strong> <?php echo $cliente->telefono; ?><br>
				                  		<strong>SERVICIO: </strong> <?php echo $cliente->getPaquete()->paquete; ?>
				                	</p>
				                	
				              	</div>
				            </div>
				            <div class="col-md-6">
				            	<div class="panel panel-default">
  									<div class="panel-heading">
  										Todos los pagos del año <strong><?php echo date("Y")  ?></strong>, Cliente: <strong><?php echo $cliente->nombre; ?></strong>
  									</div>
  									<div class="panel-body">
	
							            <div class="col-md-6">
<?php 
	$pagada = '<span class="label label-success">Pagada</span>';
	$vencida = '<span class="label label-warning">Pendiente</span>';
?>
<p>
	<?php $existePagoCliente01 = PagoData::ExistPayment_MonthDay(01,$cliente->cliente_id); ?>
	<strong>Enero <?php echo (count($existePagoCliente01)>0) ? $pagada : $vencida ?></strong><br>
	
	<?php $existePagoCliente02 = PagoData::ExistPayment_MonthDay(02,$cliente->cliente_id); ?>
	<strong>Febrero <?php echo (count($existePagoCliente02)>0) ? $pagada : $vencida ?></strong><br>
	
	<?php $existePagoCliente03 = PagoData::ExistPayment_MonthDay(03,$cliente->cliente_id); ?>
	<strong>Marzo <?php echo (count($existePagoCliente03)>0) ? $pagada : $vencida ?></strong><br>
	
	<?php $existePagoCliente04 = PagoData::ExistPayment_MonthDay(04,$cliente->cliente_id); ?>
	<strong>Abril <?php echo (count($existePagoCliente04)>0) ? $pagada : $vencida ?></strong><br>
	
	<?php $existePagoCliente05 = PagoData::ExistPayment_MonthDay(05,$cliente->cliente_id); ?>
	<strong>Mayo <?php echo (count($existePagoCliente05)>0) ? $pagada : $vencida ?></strong><br>
	
	<?php $existePagoCliente06 = PagoData::ExistPayment_MonthDay(06,$cliente->cliente_id); ?>
	<strong>Junio <?php echo (count($existePagoCliente06)>0) ? $pagada : $vencida ?></strong><br>
</p>
							            </div>
							            <div class="col-md-6">
<p>
	<?php $existePagoCliente07 = PagoData::ExistPayment_MonthDay(07,$cliente->cliente_id); ?>
	<strong>Julio <?php echo (count($existePagoCliente07)>0) ? $pagada : $vencida ?></strong><br>

	<?php $existePagoCliente08 = PagoData::ExistPayment_MonthDay(8,$cliente->cliente_id); ?>
	<strong>Agosto <?php echo (count($existePagoCliente08)>0) ? $pagada : $vencida ?></strong><br>

	<?php $existePagoCliente09 = PagoData::ExistPayment_MonthDay(9,$cliente->cliente_id); ?>
	<strong>Septiembre <?php echo (count($existePagoCliente09)>0) ? $pagada : $vencida ?></strong><br>

	<?php $existePagoCliente10 = PagoData::ExistPayment_MonthDay(10,$cliente->cliente_id); ?>
	<strong>Octubre <?php echo (count($existePagoCliente10)>0) ? $pagada : $vencida ?></strong><br>

	<?php $existePagoCliente11 = PagoData::ExistPayment_MonthDay(11,$cliente->cliente_id); ?>
	<strong>Noviembre <?php echo (count($existePagoCliente11)>0) ? $pagada : $vencida ?></strong><br>

	<?php $existePagoCliente012 = PagoData::ExistPayment_MonthDay(12,$cliente->cliente_id); ?>
	<strong>Diciembre <?php echo (count($existePagoCliente012)>0) ? $pagada : $vencida ?></strong><br>

</p>
							            </div>
				            		</div>
								</div>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-md-6 text-center">
				             	<div class="well">
				             		<?php $configTax = BusinessData::getById(1); ?>
				              		<h1>Total a cobrar: <br>
						                <?php
						                	if ($cliente->iva == 1) {
						                  		echo number_format($cliente->getPaquete()->precio*$configTax->tax,2,'.','');
						                ?>
						                	<input type="hidden" name="pago" value="<?php echo $cliente->getPaquete()->precio*$configTax->tax; ?>">
						                <?php
						                	}else{
						                  	echo number_format($cliente->getPaquete()->precio,2,'.','');
						                ?>
						                <input type="hidden" name="pago" value="<?php echo $cliente->getPaquete()->precio; ?>">
						                <?php
						                	}
						                ?>
				              		</h1>
			            		</div>
				          	</div>

							
					        <div class="col-md-6">
					            <div class="form-group">
				              		<!-- <label>EFECTIVO</label> -->
				              		<select class="form-control input-lg" name="metodo_pago" id="metodo_pago" required="required">
				              			<option value="">---FORMA DE PAGO---</option>
				              			<option value="1">EFECTIVO</option>
				              			<option value="2">TRANSFERENCIA</option>
				              		</select>

				              		<div  style="display: none" id="container_num_deposito">
					              		<br>
				              			<input type="text" class="form-control" id="num_deposito" name="num_deposito" placeholder="# Deposito">
				              		</div>
					            </div>
					            <!-- <span class="text-muted">Pagos del año <?php echo date("Y") ?></span> -->
					            <div class="form-group">
			              			<select class="form-control input-lg" name="date" required="required">
			              				<option value="01">Enero</option>
			              				<option value="02">Febrero</option>
			              				<option value="03">Marzo</option>
			              				<option value="04">Abril</option>
			              				<option value="05">Mayo</option>
			              				<option value="06">Junio</option>
			              				<option value="07">Julio</option>
			              				<option value="08">Agosto</option>
			              				<option value="09">Septiembre</option>
			              				<option value="10">Octubre</option>
			              				<option value="11">Noviembre</option>
			              				<option value="12">Diciembre</option>
			              			</select>
			              			<br><br>
			              			<input type="text" class="form-control input-lg" name="efectivo" value="<?php echo isset($_POST['efectivo']) ?>" required="" placeholder="$ 0.00" required="required">
							        <input type="hidden" class="form-control" name="fecha_inicio" value="<?php echo date('Y-m-d'); ?>" required="">
							        <input type="hidden" class="form-control" name="hora" value="<?php echo date('H:i:s'); ?>" required="">
							        <input type="hidden" name="idcliente" value="<?php echo $cliente->cliente_id; ?>">
							        <input type="hidden" name="idpaquete" value="<?php echo $cliente->idpaquete; ?>">
					            </div>
					        </div>

					        <!-- </div> -->


				    	</div>
					</div>
					<div class="box-footer">
				        <button type="submit" class="btn btn-primary btn-block btn-lg"><i class="fa fa-spinner"></i> COBRAR</button>
				    </div>
				</form>
				</div>
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
	$(document).ready(function(){
		$("#metodo_pago").change(function(){
			if ($(this).val()==2) {
				$("#container_num_deposito").show();
			}else{
				$("#container_num_deposito").hide();
			}
		})
		if ($(this).val()==2) {
			$("#container_num_deposito").show();
		}else{
			$("#container_num_deposito").hide();
		}
	})
</script>