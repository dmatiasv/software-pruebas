<?php 
	#codigo para eliminar
	if (isset($_REQUEST["id"])){
		$id=$_REQUEST["id"];
		$id=intval($id);

		$delete = ClienteData::getById($id);

			$del = $delete->del();
			//print_r($del);
			if($del[0]==1){
				$aviso="Bien hecho!";
				$msj="Datos eliminados satisfactoriamente.";
				$classM="alert alert-success";
				$times="&times;";
			}else{
				$aviso="Aviso!";
				$msj="Error al eliminar los datos.";
				$classM="alert alert-danger";
				$times="&times;";
			}
		
	}
?>
<?php
	$query = Database::cleanChain($_REQUEST['query']);
	
	$sWhere=" estado=0 and";
	$sWhere.=" (nombre LIKE '%".$query."%' ";
	$sWhere.=" or telefono LIKE '%".$query."%') ";
	$sWhere.=" order by cliente_id asc";

	include "app/pagination.php"; //incluyo el archivo de paginación

	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
	$per_page = intval($_REQUEST['per_page']); //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;

	$count_query=ClienteData::countQuery($sWhere);
	$numrows = $count_query->numrows;
	$total_pages = ceil($numrows/$per_page);
	$reload = "cobrar/index";

	$query=ClienteData::query($sWhere, $offset,$per_page);
?>
<?php 
	if (isset($_REQUEST["id"])){
?>
	<div class="<?php echo $classM;?>">
		<button type="button" class="close" data-dismiss="alert"><?php echo $times;?></button>
		<strong><?php echo $aviso?> </strong>
		<?php echo $msj;?>
	</div>	
<?php 
	}
	// si hay registro
	if(count($query)>0){ 
?>

<ul class="list-group">
	<?php
      	$finales=0; 
      	foreach ($query as $cliente): 

      		//calcular por el dia si es mayor al actual, si es mayor ya vencio; SI no esta en el rango de fecha de pago

			/*$fecha_f = ClienteData::getByIdDateDiff($cliente->cliente_id);
			echo $fecha_f->dias_faltantes;
			if ($fecha_f->dias_faltantes>=1 and $fecha_f->dias_faltantes<=4) {
				$class_txt="danger";
				$icon="<i class='text-red fa fa-lg fa-exclamation-triangle'></i>";
			}*/




            /*$daterange = "01".date('/m/Y').' - '.date('d/m/Y');
            $pagosClientes = PagoData::getClientDaterange($cliente->cliente_id,$daterange);
			$pagarEsteMes = false;
			if (count($pagosClientes)>0) {
				$pagarEsteMes=true;
			}*/

			$pagarEsteMes=false;
			$existePagoCliente = PagoData::ExistPayment_MonthDay(date("m"),$cliente->cliente_id);
			if (count($existePagoCliente)>0) {
				$pagarEsteMes = true;
			}

			list($date)=explode(" ",$cliente->fecha_mensualidad);
            list($Y,$m,$d)=explode("-",$date);
            $date=$d."/".$m."/".$Y;


            if ($d<date("d") and !$pagarEsteMes) {
            	$spanBg = "bg-red";
            	$spanText = "Vencida";
            }else if($d===date("d") and !$pagarEsteMes){
            	$spanBg = "bg-primary";
            	$spanText = "Pago hoy";
            }else if($d>date("d") and !$pagarEsteMes){
            	$spanBg = "bg-yellow";
            	$spanText = "Pendiente";
            }else if($pagarEsteMes){
            	$spanBg = "bg-green";
            	$spanText = "Pagada";
            }


      		$finales++;
    ?>
    <li class="list-group-item list-group-item- danger <?php echo $class_txt; ?>">
    	
        <a href="cobrar/edit/<?php echo $cliente->cliente_id; ?>" title="Realizar cobro">
          	<h4 class="list-group-item-heading"><?php echo $cliente->nombre; ?></h4>
          	<p class="list-group-item-text">
            	Dirección: <?php echo $cliente->direccion; ?><br>
            	Telefono: <?php echo $cliente->telefono; ?><br>
            	Servicio contratado: <?php echo $cliente->getPaquete()->paquete; ?> $<?php echo $cliente->getPaquete()->precio; ?><br>
            	<!-- Próximo Pago: <?php $fecha_f->dias_faltantes ?>  -->
            	Estado de pago:&nbsp; <span class="label <?php echo $spanBg ?>"><?php echo $spanText ?></span>
          	</p>
        </a>
    </li>
    <br>
    <?php endforeach; ?>
</ul>

<?php
	}else{
?>
	<div class="alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<strong>Sin Resultados!</strong> No se encontraron resultados en la base de datos!.
	</div>
<?php
	}
?>