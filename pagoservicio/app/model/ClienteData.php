<?php
class ClienteData {
	public static $tablename = "clientes";

	public function __construct(){
		$this->nombre = "";
		$this->rfc = "";
		$this->direccion = "";
		$this->telefono = "";
		$this->correo = "";
		$this->fecha_contratacion = "";
		$this->fecha_mensualidad = "";
		$this->idpaquete = "";
		$this->iva = "";
		$this->notas = "";
		$this->estado = "";
	}

	public function getPaquete(){ return PaqueteData::getById($this->idpaquete);}

	public function add(){
		$sql = "insert into clientes (nombre, rfc,direccion,telefono,correo,fecha_contratacion,fecha_mensualidad,idpaquete,iva,notas,estado) ";
		$sql .= "values (\"$this->nombre\",\"$this->rfc\",\"$this->direccion\",\"$this->telefono\",\"$this->correo\",\"$this->fecha_contratacion\",\"$this->fecha_mensualidad\",\"$this->idpaquete\",\"$this->iva\",\"$this->notas\",\"$this->estado\")";
		Executor::doit($sql);
	}

	public function del(){
		$sql = "delete from ".self::$tablename." where cliente_id=$this->id";
		return Executor::doit($sql);
	}

	public static function delBy($k,$v){
		$sql = "delete from ".self::$tablename." where $k=\"$v\"";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",rfc=\"$this->rfc\",direccion=\"$this->direccion\",telefono=\"$this->telefono\",correo=\"$this->correo\",fecha_contratacion=\"$this->fecha_contratacion\",fecha_mensualidad=\"$this->fecha_mensualidad\",idpaquete=\"$this->idpaquete\",iva=\"$this->iva\",notas=\"$this->notas\",estado=\"$this->estado\" where cliente_id=$this->cliente_id";
		Executor::doit($sql);
	}

	public function updateById($k,$v){
		$sql = "update ".self::$tablename." set $k=\"$v\" where cliente_id=$this->cliente_id";
		Executor::doit($sql);
	}

	public static function getById($id){
		 $sql = "select * from ".self::$tablename." where cliente_id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ClienteData());
	}

	public static function getByIP($ip){
		 $sql = "select * from ".self::$tablename." where serie=\"$ip\"";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ClienteData());
	}

	public static function getByIdDateDiff($id){
		$fecha_actual=date('Y-m-d');
		$sql = "SELECT DATEDIFF(fecha_mensualidad,\"$fecha_actual\") as 'dias_faltantes' from ".self::$tablename." where cliente_id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ClienteData());
	}

	public static function getBy($k,$v){
		$sql = "select * from ".self::$tablename." where $k=\"$v\"";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ClienteData());
	}

	public static function getAll(){
		 $sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new ClienteData());
	}

	public static function getAllBy($k,$v){
		 $sql = "select * from ".self::$tablename." where $k=\"$v\"";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ClienteData());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where antena like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ClienteData());
	}

	public static function countQuery($where){
		$sql = "SELECT count(*) AS numrows FROM ".self::$tablename." where ".$where;
		$query = Executor::doit($sql);
		return Model::one($query[0],new ClienteData());
	}

	public static function query($sWhere, $offset,$per_page){
		$sql = "SELECT * FROM ".self::$tablename." where ".$sWhere." LIMIT $offset,$per_page";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ClienteData());
	}
}

?>